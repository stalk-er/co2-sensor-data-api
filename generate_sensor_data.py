from argparse import ArgumentParser

from random import randint

import calendar
import time

import requests

json_obj = {
    "from_ts": -1,
    "to_ts": -1,
    "entries": {
        "voc": {
            "open": "100.00",
            "high": "120.00",
            "low": "20.00",
            "close": "50.00"
        },
        "eco2": {
            "open": "100.00",
            "high": "120.00",
            "low": "20.00",
            "close": "50.00"
        },
        "temp": {
            "open": "100.00",
            "high": "120.00",
            "low": "20.00",
            "close": "50.00"
        },
        "humidity": {
            "open": "100.00",
            "high": "120.00",
            "low": "20.00",
            "close": "50.00"
        },
        "barometric_pressure": {
            "open": "100.00",
            "high": "120.00",
            "low": "20.00",
            "close": "50.00"
        }
    }
}

parser = ArgumentParser(description='Generate customer device sensor data for testing purposes.')
parser.add_argument('--customer-id', help='The customer id who owns the device.', required=True)
parser.add_argument('--device-id', help='The device id for which to generate sensor data.', required=True)
# parser.print_help()
args = parser.parse_args()

customer_id = args.customer_id
device_id = args.device_id

starting_timestamp = calendar.timegm(time.gmtime())
to_ts = starting_timestamp

print('Starting timestamp', starting_timestamp)

counter = 1
while True:

    from_ts = to_ts
    to_ts = from_ts + 60

    json_obj['from_ts'] = from_ts
    json_obj['to_ts'] = to_ts
    for key, value in json_obj['entries'].items():
        value['open'] = str(randint(0, 100))
        value['high'] = str(randint(0, 100))
        value['low'] = str(randint(0, 100))
        value['close'] = str(randint(0, 100))
        requests.post(
          'http://localhost/customer/' + customer_id + '/devices/' + device_id + '/store/',
          json=json_obj
        )

    counter += 1
    if counter >= 60*24*31:
        break

print('Final timestamp', to_ts)
