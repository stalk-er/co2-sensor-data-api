import logging

from bson.json_util import dumps
from flask import make_response
from flask_restful import Resource, reqparse

from jwt_utils import jwt_required
from myapp import db

parser = reqparse.RequestParser()
parser.add_argument('from', help='This field cannot be blank', required=True)
parser.add_argument('to', help='This field cannot be blank', required=True)


class DeviceDataFetch(Resource):
    @staticmethod
    @jwt_required
    def post(customer_id, device_id):
        post_data = parser.parse_args()

        collection_name = "%s-%s" % (customer_id, device_id)
        collection = db[collection_name]

        batch_size = 12
        from_datetime = post_data['from']
        to_datetime = post_data['to']

        if from_datetime > to_datetime:
            return make_response(dumps({
                'status': 'error',
                'message': 'From timestamp should be smaller than To timestamp.'
            }), 400)

        try:
            # get device data in a datetime range
            device_data = collection.find({
                'ts': {
                    '$gte': from_datetime,
                    '$lte': to_datetime,
                }
            },
            {
                '_id': False
            },
                batch_size=batch_size
            )

            def generate_response():
                for device_data_chunk in device_data:
                    yield device_data_chunk

            return make_response(dumps(generate_response()), 200)
        except Exception as ex:
            logging.error(ex)
            return make_response(dumps({
                'status': 'error',
                'message': 'Internal server error.'
            }), 500)
