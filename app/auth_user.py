from flask_restful import Resource, reqparse

parser = reqparse.RequestParser()
parser.add_argument('email', help='This field cannot be blank', required=True)
parser.add_argument('password', help='This field cannot be blank', required=True)


class AuthUser(Resource):
    def get(self):
        data = parser.parse_args()
        # db.todo.insert_one(data)
        return data
