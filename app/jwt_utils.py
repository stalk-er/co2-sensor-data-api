import datetime
from functools import wraps

import jwt
import logging

from bson import ObjectId
from flask import request, make_response, jsonify

from myapp import application


def decode_auth_token(auth_token):
    """
    Decodes the auth token
    :param auth_token:
    :return: integer|string
    """
    try:
        payload = jwt.decode(auth_token, application.config.get('SECRET_KEY'), verify=True, algorithms=[
            'HS256'
        ])
        return payload['sub']
    except jwt.ExpiredSignatureError as e:
        logging.error(e)
        return 'Signature expired. Please log in again.'
    except jwt.InvalidTokenError as e:
        logging.error(e)
        return 'Invalid token. Please log in again.'


def encode_auth_token(user_id):
    """
    Generates the Auth Token
    :return: string
    """
    try:
        payload = {
            'exp': datetime.datetime.utcnow() + datetime.timedelta(days=5, seconds=0),
            'iat': datetime.datetime.utcnow(),
            'sub': user_id
        }

        return jwt.encode(
            payload,
            application.config.get('SECRET_KEY'),
            algorithm='HS256'
        )
    except Exception as e:
        logging.error(e)
        return e


# Here is a custom decorator that verifies the JWT is present in
# the request, as well as insuring that this user has a role of
# `admin` in the access token
def jwt_required(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        logging.error('jwt_required')
        error_response = make_response(jsonify({
                    'status': 'fail',
                    'message': 'You do not have valid JWT'
                }), 401)
        auth_token = request.headers.get('Authorization').strip()
        
        if auth_token:
            resp = decode_auth_token(auth_token)
            if ObjectId.is_valid(resp):
                return fn(*args, **kwargs)
            else:
                return error_response

    return wrapper
