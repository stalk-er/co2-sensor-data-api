import logging

from bson import ObjectId
from flask import request, make_response, jsonify
from flask_restful import Resource, reqparse

from jwt_utils import decode_auth_token


class AuthLogout(Resource):
    """
    Logout Resource
    """

    @staticmethod
    def post():
        # get auth token
        auth_header = request.headers.get('Authorization')
        logging.warning(auth_header)

        if auth_header and " " in auth_header:
            auth_token = auth_header.split(" ")[1]
        else:
            auth_token = ''

        if auth_token:
            resp = decode_auth_token(auth_token)
            if ObjectId.is_valid(resp):
                return make_response(jsonify({
                    'status': 'success',
                    'message': 'Successfully logged out.'
                }), 200)
            else:
                return make_response(jsonify({
                    'status': 'fail',
                    'message': resp
                }), 401)
        else:
            return make_response(jsonify({
                'status': 'fail',
                'message': 'Provide a valid auth token.'
            }), 403)
