import logging

import bcrypt
from flask_restful import Resource, reqparse

from myapp import db
from jwt_utils import encode_auth_token
from flask import make_response, jsonify

parser = reqparse.RequestParser()
parser.add_argument('email', help='This field cannot be blank', required=True)
parser.add_argument('password', help='This field cannot be blank', required=True)


class AuthRegister(Resource):
    @staticmethod
    def post():
        post_data = parser.parse_args()
        user_data = {
            'email': post_data['email'],
            'password': bcrypt.hashpw(
                post_data['password'].encode('utf-8'),
                bcrypt.gensalt()
            )
        }
        existing_user = db.users.find_one(user_data)
        if not existing_user:
            try:
                inserted_user = db.users.insert_one(user_data)
                inserted_user_id = str(inserted_user.inserted_id) + ''
                auth_token = encode_auth_token(inserted_user_id)
                return make_response(jsonify({
                    'status': 'success',
                    'message': 'Successfully registered.',
                    'auth_token': auth_token
                }), 201)
            except Exception as e:
                logging.error(e)
                return make_response(jsonify({
                    'status': 'fail',
                    'message': 'An error occurred. Please try again, later.'
                }), 401)

        return make_response(jsonify({
            'status': 'info',
            'message': 'User is already registered. Please login.',
        }), 202)
