from flask_pymongo import PyMongo


def init_mongo(application):
    mongo = PyMongo(application)
    return mongo.db
