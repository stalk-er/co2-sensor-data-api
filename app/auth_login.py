import logging

import bcrypt
from flask import make_response, jsonify
from flask_restful import Resource, reqparse

from jwt_utils import encode_auth_token
from myapp import db

parser = reqparse.RequestParser()
parser.add_argument('email', help='This field cannot be blank', required=True)
parser.add_argument('password', help='This field cannot be blank', required=True)


class AuthLogin(Resource):
    @staticmethod
    def post():
        post_data = parser.parse_args()
        try:
            inserted_user = db.users.find_one({
                'email': post_data['email']
            })

            if inserted_user and bcrypt.checkpw(
                    post_data.get('password').encode('utf-8'), inserted_user['password']
            ):
                inserted_user_id = str(inserted_user['_id']) + ''
                auth_token = encode_auth_token(inserted_user_id)

                if auth_token:
                    return make_response(jsonify({
                        'status': 'success',
                        'message': 'Successfully logged in.',
                        'auth_token': auth_token
                    }), 200)
            else:
                return make_response(jsonify({
                    'status': 'fail',
                    'message': 'User does not exist.'
                }), 404)
        except Exception as e:
            logging.error(e)
            return make_response(jsonify({
                'status': 'fail',
                'message': 'Try again'
            }), 500)
