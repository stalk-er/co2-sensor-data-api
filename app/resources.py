from device_data_store import DeviceDataStore
from device_data_fetch import DeviceDataFetch
from device_single import DeviceSingle
from devices import Devices

from auth_logout import AuthLogout
from auth_login import AuthLogin
from auth_register import AuthRegister

from myapp import api

api.add_resource(AuthRegister, '/auth/register')
api.add_resource(AuthLogin, '/auth/login')
api.add_resource(AuthLogout, '/auth/logout')
api.add_resource(Devices, '/customer/<customer_id>/devices', methods=['GET', 'POST'])
api.add_resource(DeviceSingle, '/customer/<customer_id>/devices/<device_id>', methods=['GET', 'PATCH', 'DELETE'])
api.add_resource(DeviceDataFetch, '/customer/<customer_id>/devices/<device_id>/fetch/', methods=['POST'])
api.add_resource(DeviceDataStore, '/customer/<customer_id>/devices/<device_id>/store/', methods=['POST'])
