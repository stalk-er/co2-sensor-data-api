import logging
import random

from bson.json_util import dumps
from flask import make_response
from flask_restful import Resource, reqparse

from jwt_utils import jwt_required
from myapp import db

parser = reqparse.RequestParser()
parser.add_argument('name', help='This field cannot be blank', required=True)
parser.add_argument('description', help='This field cannot be blank', required=True)
parser.add_argument('geolocation_data', help='This field cannot be blank', required=True)
parser.add_argument('device_id', help='This field is optional', required=False)


class Devices(Resource):
    @staticmethod
    @jwt_required
    def get(customer_id):

        try:
            # return all devices
            customer_devices = db.devices.find({
                'customer_id': customer_id
            },
                {
                    '_id': False
                })

            return make_response(dumps(list(customer_devices)), 200)
        except Exception as ex:
            logging.error(ex)
            return make_response(dumps({
                'status': 'error',
                'message': 'Internal server error.'
            }), 500)

    @staticmethod
    @jwt_required
    def post(customer_id):
        post_data = parser.parse_args()

        device_already_exists = db.devices.count_documents({
            'customer_id': customer_id,
            'device_id': post_data['device_id']
        }) > 0

        if device_already_exists:
            return make_response(dumps({
                'status': 'conflict',
                'message': 'The device already exists.'
            }), 409)

        try:
            random_mac = "02:00:00:%02x:%02x:%02x" % (
                random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
            db.devices.insert({
                'customer_id': customer_id,
                'name': post_data['name'],
                'description': post_data['description'],
                'geolocation_data': post_data['geolocation_data'],
                'device_id': post_data['device_id'] or random_mac
            })

            return make_response(dumps({
                'customer_id': customer_id,
                'name': post_data['name'],
                'description': post_data['description'],
                'geolocation_data': post_data['geolocation_data'],
                'device_id': post_data['device_id'] or random_mac
            }), 201)
        except Exception as e:
            logging.error(e)
            return make_response(dumps({
                'status': 'error',
                'message': 'Internal server error.'
            }), 500)
