import os

from flask import Flask
from flask_restful import Api
from flask_pymongo import PyMongo

application = Flask(__name__)

application.config["MONGO_URI"] = 'mongodb://' + os.environ['MONGODB_USERNAME'] + ':' + os.environ[
    'MONGODB_PASSWORD'] + '@' + os.environ['MONGODB_HOSTNAME'] + ':27017/' + os.environ['MONGODB_DATABASE']

application.config['SECRET_KEY'] = 'RH@2xECGx>K>$eL'

application.config['JWT_SECRET_KEY'] = 'w3W#~@D^=7WV|@w'

# application.config['JWT_BLACKLIST_ENABLED'] = True

# application.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']

api = Api(application)
mongo = PyMongo(application)
db = mongo.db

import resources