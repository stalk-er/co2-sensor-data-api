import logging

from bson.json_util import dumps
from flask import make_response
from flask_restful import Resource, reqparse

from jwt_utils import jwt_required
from myapp import db

parser = reqparse.RequestParser()
parser.add_argument('eco2', help='This field cannot be blank', required=True)
parser.add_argument('voc', help='This field cannot be blank', required=True)
parser.add_argument('humidity', help='This field cannot be blank', required=True)
parser.add_argument('pressure', help='This field cannot be blank', required=True)
parser.add_argument('temp', help='This field cannot be blank', required=True)
parser.add_argument('ts', help='This field cannot be blank', required=True)

class DeviceDataStore(Resource):
    @staticmethod
    @jwt_required
    def post(customer_id, device_id):
        # entries_inner_args = entries_inner_parser.parse_args(req=entries_parser.parse_args(req=parser.parse_args()))
        payload = parser.parse_args()

        logging.error(payload)
        collection_name = "%s-%s" % (customer_id, device_id)
        collection = db[collection_name]

        try:
            collection.insert_one(payload)
            return make_response(dumps({}), 201)
        except Exception as ex:
            logging.error(ex)
            return make_response(dumps({
                'status': 'error',
                'message': 'Internal server error.'
            }), 500)
