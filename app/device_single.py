import logging

from bson.json_util import dumps
from flask import make_response
from flask_restful import Resource, reqparse

from jwt_utils import jwt_required
from myapp import db

parser = reqparse.RequestParser()
parser.add_argument('name', help='This field cannot be blank', required=True)
parser.add_argument('description', help='This field cannot be blank', required=True)
parser.add_argument('geolocation_data', help='This field cannot be blank', required=True)
parser.add_argument('device_id', help='This field cannot be blank', required=True)


class DeviceSingle(Resource):
    @staticmethod
    @jwt_required
    def get(customer_id, device_id):

        try:
            # return a specified device
            customer_device = db.devices.find_one({
                'customer_id': customer_id,
                'device_id': device_id
            },
                {
                    '_id': False
                })
            return make_response(dumps(customer_device), 200)
        except Exception as ex:
            logging.error(ex)
            return make_response(dumps({
                'status': 'error',
                'message': 'Internal server error.'
            }), 500)

    @staticmethod
    @jwt_required
    def patch(customer_id, device_id):
        post_data = parser.parse_args()

        try:
            db.devices.remove({
                'customer_id': customer_id,
                'device_id': device_id
            },
                dict(post_data)
            )
            return make_response(204)
        except Exception as e:
            logging.error(e)
            return make_response(dumps({
                'status': 'error',
                'message': 'Internal server error.'
            }), 500)

    @staticmethod
    @jwt_required
    def delete(customer_id, device_id):
        try:
            if db.devices.find_one({
                'customer_id': customer_id,
                'device_id': device_id
            }):
                db.devices.remove({
                    'customer_id': customer_id,
                    'device_id': device_id
                })
                return make_response(dumps({}), 204)

            return make_response(dumps({
                'status': 'fail',
                'message': 'Entity not found.'
            }), 404)
        except Exception as e:
            logging.error(e)
            return make_response(dumps({
                'status': 'error',
                'message': 'Internal server error.'
            }), 500)
