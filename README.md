## CO2 sensor data API

Installation:

- Clone the project in a preferred directory
- Go to the project root and type `docker-compose up -d`. This command will download all dependencies and build everything for you.
- To log in to a container use the follow command `docker exec -it {container name} /bin/bash`
- To see logs of a container use the following command `docker logs {container name}`
- To restart a container use the following command `docker restart {container name}`
- To generate sensors data for a given device use the following command `python generate_sensor_data.py --customer-id={your customer id} --device-id={your device id}`. The data generated is for 31 days, 1 record is considered to be for 1 minute.
- Set up db and user in mongodb: `mongo -u mongodbuser -p123456789`, `use flaskdb`, `db.createUser({user: 'flaskuser', pwd: '123456789', roles: [{role: 'readWrite', db: 'flaskdb'}]})`


Resources:

- https://www.digitalocean.com/community/tutorials/how-to-set-up-flask-with-mongodb-and-docker
- Connect to the DB: mongo -u flaskuser -p 123456789 --authenticationDatabase flaskdb
